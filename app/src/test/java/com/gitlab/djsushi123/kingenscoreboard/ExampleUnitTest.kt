package com.gitlab.djsushi123.kingenscoreboard

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun `addition is correct`() {
        assertEquals(4, 2 + 2)
    }
}