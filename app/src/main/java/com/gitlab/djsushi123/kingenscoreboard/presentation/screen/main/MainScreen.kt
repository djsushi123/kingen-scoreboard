package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.main

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.gitlab.djsushi123.kingenscoreboard.common.debug
import com.gitlab.djsushi123.kingenscoreboard.presentation.BottomBarNavDestination
import com.gitlab.djsushi123.kingenscoreboard.presentation.NavDestination

@Composable
fun MainScreen(
    appNavController: NavHostController,
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    val currentBottomBarNavDestination: BottomBarNavDestination? =
        BottomBarNavDestination.from(currentBackStackEntry?.destination?.route)

    Scaffold(
        bottomBar = {
            BottomBar(
                currentDestination = currentBottomBarNavDestination,
                navController = navController
            )
        },
        floatingActionButton = {
            FAB(
                currentDestination = currentBottomBarNavDestination,
                onNewGameClicked = {
                    appNavController.navigate(NavDestination.NewGame.route)
                },
                onAddPlayerClicked = {
                    appNavController.navigate(NavDestination.NewPlayer.route)
                }
            )
        },
        modifier = modifier
    ) { paddingValues ->
        MainScreenNavGraph(navController, paddingValues)
    }
}

@Composable
private fun BottomBar(
    navController: NavHostController,
    currentDestination: BottomBarNavDestination?
) {
    BottomNavigation {
        for (destination in BottomBarNavDestination.values()) {
            BottomNavigationItem(
                selected = currentDestination == destination,
                onClick = onClick@{
                    if (currentDestination == destination) return@onClick

                    navController.navigate(destination.route) {
                        debug("Navigated to ${destination.route}")
                        launchSingleTop = true
                        popUpTo(
                            navController.graph.findStartDestination().route
                                ?: BottomBarNavDestination.Scoreboard.route
                        )
                    }
                    debug("Backstack: ${navController.backQueue.map { it.destination.route }}")
                },
                icon = { Icon(destination.icon, destination.label) },
                label = { Text(text = destination.label) }
            )
        }
    }
}

@Composable
private fun FAB(
    currentDestination: BottomBarNavDestination?,
    onNewGameClicked: () -> Unit,
    onAddPlayerClicked: () -> Unit,
) {
    when (currentDestination) {
        BottomBarNavDestination.ActiveGames -> FloatingActionButton(onClick = onNewGameClicked) {
            Icon(Icons.Default.Add, "New game")
        }
        BottomBarNavDestination.Players -> FloatingActionButton(onClick = onAddPlayerClicked) {
            Icon(Icons.Default.Add, "New player")
        }
        else -> {}
    }
}