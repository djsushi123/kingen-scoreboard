package com.gitlab.djsushi123.kingenscoreboard.presentation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FormatListNumbered
import androidx.compose.material.icons.filled.GridOn
import androidx.compose.material.icons.filled.Group
import androidx.compose.material.icons.filled.History
import androidx.compose.ui.graphics.vector.ImageVector

sealed class NavDestination(
    open val route: String
) {
    object Main : NavDestination(route = "main_screen")
    object NewGame : NavDestination(route = "new_game_screen")
    object NewPlayer : NavDestination(route = "new_player_screen")
}

open class BottomBarNavDestination(
    val label: String,
    override val route: String,
    val icon: ImageVector
) : NavDestination(route = route) {

    object ActiveGames : BottomBarNavDestination(
        label = "Active",
        route = "active_games_screen",
        icon = Icons.Default.FormatListNumbered
    )

    object Scoreboard : BottomBarNavDestination(
        label = "Scoreboard",
        route = "scoreboard_screen",
        icon = Icons.Default.GridOn
    )

    object Players : BottomBarNavDestination(
        label = "Players",
        route = "players_screen",
        icon = Icons.Default.Group
    )

    object History : BottomBarNavDestination(
        label = "History",
        route = "history_screen",
        icon = Icons.Default.History
    )

    companion object {
        fun values(): List<BottomBarNavDestination> =
            listOf(ActiveGames, Scoreboard, Players, History)

        fun from(route: String?) = values().firstOrNull { it.route == route }
    }
}