package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.scoreboard

import com.gitlab.djsushi123.kingenscoreboard.domain.model.Round

data class Score(
    val gameId: Int,
    val playerId: Int,
    val round: Round,
    val score: Int
)