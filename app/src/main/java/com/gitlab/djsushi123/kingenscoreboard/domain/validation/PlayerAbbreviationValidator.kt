package com.gitlab.djsushi123.kingenscoreboard.domain.validation

import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository

class PlayerAbbreviationValidator(private val playerRepository: PlayerRepository) {

    suspend fun execute(abbreviation: String): ValidationResult {
        var errorMessage: String? = null

        if (abbreviation.length != 2) errorMessage = "Abbreviation has to be 2 letters or digits."

        val players = playerRepository.getAllPlayers()
        val playerWithSameNameExists = players.any { it.abbreviation == abbreviation }
        if (playerWithSameNameExists) {
            errorMessage = "Player with the same abbreviation already exists."
        }

        return ValidationResult(successful = errorMessage == null, errorMessage = errorMessage)
    }

}