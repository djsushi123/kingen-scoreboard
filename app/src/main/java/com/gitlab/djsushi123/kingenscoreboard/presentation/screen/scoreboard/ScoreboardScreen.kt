package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.scoreboard

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import org.koin.androidx.compose.getViewModel

@Composable
fun ScoreboardScreen(
    viewModel: ScoreboardScreenViewModel = getViewModel(),
    modifier: Modifier = Modifier
) {
    // The whole grid
    Column(modifier = modifier.fillMaxSize()) {
        // Players row
        GridRow {
            // Empty cell in the top left
            Cell()

            @Suppress("KotlinConstantConditions")
            for (player in emptyList<Player>() /*TODO players*/) {
                Cell(
                    text = player.abbreviation,
                    modifier = Modifier.background(player.color)
                )
            }
        }
        for (row in 0 until 10) {
            GridRow {
                for (column in 0 until 5) {
                    Cell(text = "HA$row $column", onClick = {})
                }
            }
        }
    }
}