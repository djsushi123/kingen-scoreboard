package com.gitlab.djsushi123.kingenscoreboard.domain.validation

import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository

class PlayerNameValidator(private val playerRepository: PlayerRepository) {

    suspend fun execute(playerName: String): ValidationResult {
        var errorMessage: String? = when {
            playerName.isBlank() -> "Name cannot be blank."
            playerName.length > 32 -> "Name cannot contain more than 32 characters."
            playerName.first().isWhitespace() -> "Name cannot start with whitespace."
            playerName.last().isWhitespace() -> "Name cannot end with whitespace."
            !playerName.all { it.isLetterOrDigit() || it.isWhitespace() } ->
                "Can only contain letters, digits and spaces."
            else -> null
        }

        val players = playerRepository.getAllPlayers()
        val playerWithSameNameExists = players.any { it.name == playerName }
        if (playerWithSameNameExists) errorMessage = "Player with the same name already exists."

        return ValidationResult(successful = errorMessage == null, errorMessage = errorMessage)
    }

}