package com.gitlab.djsushi123.kingenscoreboard.data.mapper

import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.ParticipantEntity
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Participant

fun ParticipantEntity.toParticipant() = Participant(
    gameId = gameId,
    playerId = playerId,
    initialOrder = initialOrder
)

fun Participant.toParticipantEntity() = ParticipantEntity(
    gameId = gameId,
    playerId = playerId,
    initialOrder = initialOrder
)

