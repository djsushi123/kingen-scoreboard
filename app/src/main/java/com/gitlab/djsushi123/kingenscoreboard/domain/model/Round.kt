package com.gitlab.djsushi123.kingenscoreboard.domain.model


enum class Round {
    MS, MH, BK, D, HK, ZL, T0, T1, T2, T3
}