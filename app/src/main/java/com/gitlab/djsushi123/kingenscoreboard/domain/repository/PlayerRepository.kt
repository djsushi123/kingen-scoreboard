package com.gitlab.djsushi123.kingenscoreboard.domain.repository

import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import kotlinx.coroutines.flow.Flow

interface PlayerRepository {

    suspend fun getAllPlayers(): List<Player>

    fun getAllPlayersAsFlow(): Flow<List<Player>>

    suspend fun insertPlayer(player: Player)

    suspend fun deletePlayer(player: Player)

    suspend fun updatePlayer(player: Player)
}