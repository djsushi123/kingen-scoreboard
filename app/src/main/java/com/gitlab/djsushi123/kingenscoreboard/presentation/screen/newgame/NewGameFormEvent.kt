package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame

sealed class NewGameFormEvent {
    data class GameNameChanged(val gameName: String) : NewGameFormEvent()
}