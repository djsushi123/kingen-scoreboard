package com.gitlab.djsushi123.kingenscoreboard

import android.app.Application
import com.gitlab.djsushi123.kingenscoreboard.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class KingenScoreboardApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@KingenScoreboardApplication)
            modules(appModule)
        }
    }

}