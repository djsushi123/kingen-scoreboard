package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer

import android.util.Log
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

val newPlayerColors: List<Color> = (0..330 step 28).map { hue ->
    Color.hsl(hue = hue.toFloat(), saturation = 0.5f, lightness = 0.5f)
}

@Composable
fun NewPlayerScreen(
    viewModel: NewPlayerScreenViewModel = getViewModel(),
    appNavController: NavHostController,
    modifier: Modifier = Modifier
) {
    val coroutineScope = rememberCoroutineScope()

    val state = viewModel.state

    val animatedSelectedColor by animateColorAsState(
        targetValue = state.playerColor,
        animationSpec = tween(durationMillis = 600)
    )

    val useDarkIcons = with(animatedSelectedColor) {
        (red + green + blue) / 3f > 0.5f
    }

    Scaffold(
        topBar = {
            TopBar(
                color = animatedSelectedColor,
                onColor = if (useDarkIcons) Color.Black else Color.White,
                saveButtonEnabled = state.saveButtonEnabled,
                onBackButtonClicked = { appNavController.popBackStack() },
                onSave = {
                    val player = Player(
                        name = state.playerName,
                        abbreviation = state.playerAbbreviation,
                        color = state.playerColor
                    )
                    coroutineScope.launch {
                        viewModel.savePlayer(player)
                        appNavController.popBackStack()
                    }
                }
            )
        },
        modifier = modifier
    ) { paddingValues ->
        Surface(color = MaterialTheme.colors.surface, modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState()) // so that we can scroll with the keyboard open
                    .padding(paddingValues)
                    .padding(start = 8.dp, top = 8.dp, end = 8.dp)
            ) {
                // Player name field with error
                OutlinedTextField(
                    value = state.playerName,
                    onValueChange = {
                        viewModel.onEvent(
                            NewPlayerFormEvent.PlayerNameChanged(name = it)
                        )
                    },
                    isError = state.playerNameError != null,
                    label = { Text(text = "Name") },
                    placeholder = { Text(text = "John Cena") },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        capitalization = KeyboardCapitalization.Words
                    ),
                    modifier = Modifier.fillMaxWidth()
                )
                Text(
                    text = state.playerNameError ?: "",
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.End,
                    modifier = Modifier.fillMaxWidth()
                )

                // Player abbreviation field
                OutlinedTextField(
                    value = state.playerAbbreviation,
                    onValueChange = {
                        viewModel.onEvent(
                            NewPlayerFormEvent.PlayerAbbreviationChanged(abbreviation = it)
                        )
                    },
                    isError = state.playerAbbreviationError != null,
                    label = { Text(text = "Abbreviation") },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        capitalization = KeyboardCapitalization.Characters,
                        autoCorrect = false
                    ),
                    modifier = Modifier.fillMaxWidth()
                )
                Text(
                    text = state.playerAbbreviationError ?: "",
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.End,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                ColorPicker(
                    selected = state.playerColor,
                    onSelect = { color ->
                        viewModel.onEvent(
                            NewPlayerFormEvent.PlayerColorChanged(color = color)
                        )
                    }
                )
            }
        }
    }
}

@Composable
private fun TopBar(
    color: Color,
    onColor: Color,
    saveButtonEnabled: Boolean,
    onBackButtonClicked: () -> Unit,
    onSave: () -> Unit
) {
    val saveButtonAlpha by animateFloatAsState(
        if (saveButtonEnabled) ContentAlpha.high else ContentAlpha.disabled
    )

    TopAppBar(
        title = { Text(text = "New player", color = onColor) },
        navigationIcon = {
            IconButton(onClick = onBackButtonClicked) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Back",
                    tint = onColor
                )
            }
        },
        actions = {
            IconButton(onClick = onSave, enabled = saveButtonEnabled) {
                Icon(
                    imageVector = Icons.Default.Check,
                    contentDescription = "Save",
                    tint = onColor.copy(alpha = saveButtonAlpha)
                )
            }
        },
        backgroundColor = color
    )
}

@Composable
private fun ColorPicker(
    selected: Color,
    onSelect: (color: Color) -> Unit,
    spacing: Dp = 8.dp,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        for (row in 0..(newPlayerColors.size - 1) / 4) {
            Row(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxWidth()
            ) {
                for (tile in 0 until 4) {
                    val color = newPlayerColors[row * 4 + tile]
                    ColorPickerTile(
                        color = color,
                        selected = color == selected,
                        onClick = { onSelect(color) },
                        modifier = Modifier
                            .width(70.dp)
                            .height(70.dp)
                    )
                    if (tile != 3) Spacer(modifier = Modifier.width(spacing))
                }
            }
            if (row != (newPlayerColors.size - 1) / 4) Spacer(modifier = Modifier.height(spacing))
        }
    }
}

@Composable
private fun ColorPickerTile(
    color: Color,
    selected: Boolean,
    onClick: () -> Unit,
    shape: Shape = MaterialTheme.shapes.small,
    modifier: Modifier = Modifier
) {
    if (color == Color.Red) Log.d("ColorPickerTile", "Recomposed with arguments: " +
            "$color, $selected, $onClick, $shape, $modifier")

    val scale = remember { Animatable(initialValue = 1f) }
    val borderWidth by animateDpAsState(
        targetValue = if (selected) 6.dp else 0.dp,
        animationSpec = tween(durationMillis = 350)
    )

    LaunchedEffect(key1 = selected) {
        if (selected) {
            launch {
                scale.animateTo(
                    targetValue = 0.90f,
                    animationSpec = tween(
                        durationMillis = 50
                    )
                )
                scale.animateTo(
                    targetValue = 1f,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            }
        }
        else {
            launch { scale.animateTo(1f) }
        }
    }

    Box(
        modifier = modifier
            .scale(scale.value)
            .border(
                width = borderWidth,
                color = if (borderWidth.value == 0f) Color.Transparent else MaterialTheme.colors.onSurface,
                shape = shape
            )
            .clip(shape)
            .background(color = color)
            .clickable(onClick = onClick)
    )
}
