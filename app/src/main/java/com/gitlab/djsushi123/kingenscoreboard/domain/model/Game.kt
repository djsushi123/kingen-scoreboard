package com.gitlab.djsushi123.kingenscoreboard.domain.model

data class Game(
    val id: Long = 0,
    val name: String
)