package com.gitlab.djsushi123.kingenscoreboard.data.repository

import com.gitlab.djsushi123.kingenscoreboard.data.local.dao.PlayerDao
import com.gitlab.djsushi123.kingenscoreboard.data.mapper.toPlayer
import com.gitlab.djsushi123.kingenscoreboard.data.mapper.toPlayerEntity
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class PlayerRepositoryImpl(private val playerDao: PlayerDao) : PlayerRepository {

    override suspend fun getAllPlayers(): List<Player> {
        return playerDao.getAllPlayers().map { it.toPlayer() }
    }

    override fun getAllPlayersAsFlow(): Flow<List<Player>> {
        return playerDao.getAllPlayersAsFlow().map { it.map { entity -> entity.toPlayer() } }
    }

    override suspend fun insertPlayer(player: Player) {
        playerDao.insertPlayer(player.toPlayerEntity())
    }

    override suspend fun deletePlayer(player: Player) {
        playerDao.deletePlayer(player.toPlayerEntity())
    }

    override suspend fun updatePlayer(player: Player) {
        playerDao.updatePlayer(player.toPlayerEntity())
    }
}