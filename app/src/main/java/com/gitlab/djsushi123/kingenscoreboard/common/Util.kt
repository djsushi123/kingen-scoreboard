package com.gitlab.djsushi123.kingenscoreboard.common

import android.util.Log

fun Any.debug(msg: String) {
    Log.d(this::class.simpleName, msg)
}