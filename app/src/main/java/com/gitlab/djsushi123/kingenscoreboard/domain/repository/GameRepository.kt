package com.gitlab.djsushi123.kingenscoreboard.domain.repository

import com.gitlab.djsushi123.kingenscoreboard.domain.model.Game
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Participant
import kotlinx.coroutines.flow.Flow

interface GameRepository {

    suspend fun getAllGames(): List<Game>

    fun getAllGamesAsFlow(): Flow<List<Game>>

    suspend fun insertGame(game: Game): Long

    suspend fun insertParticipants(participants: List<Participant>)

}
