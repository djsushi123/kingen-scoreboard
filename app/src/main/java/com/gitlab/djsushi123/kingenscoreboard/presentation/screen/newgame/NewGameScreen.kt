package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame

import androidx.compose.animation.core.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Game
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Participant
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import com.gitlab.djsushi123.kingenscoreboard.presentation.NavDestination
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun NewGameScreen(
    appNavController: NavHostController,
    viewModel: NewGameScreenViewModel = getViewModel(),
    modifier: Modifier = Modifier
) {
    val coroutineScope = rememberCoroutineScope()
    val state = viewModel.state
    val playersState by viewModel.playerFlow.collectAsState(initial = emptyList())

    // We remove the overlap between playersState and the selected players and then we add the
    // selected players at the beginning of the list
    val players = state.selectedPlayers + (playersState - state.selectedPlayers.toSet())

    Scaffold(
        topBar = {
            TopBar(
                saveButtonEnabled = state.saveButtonEnabled,
                onBackButtonClicked = { appNavController.popBackStack() },
                onSave = {
                    val game = Game(name = state.gameName)
                    val participants = state.selectedPlayers.mapIndexed { index, player ->
                        Participant(gameId = 0L, playerId = player.id, initialOrder = index)
                    }
                    coroutineScope.launch {
                        viewModel.saveGameAndParticipants(game, participants)
                        appNavController.popBackStack()
                    }
                }
            )
        },
        modifier = modifier
    ) { paddingValues ->
        Surface(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues)
                    .padding(start = 8.dp, top = 8.dp, end = 8.dp)
            ) {
                OutlinedTextField(
                    value = state.gameName,
                    onValueChange = {
                        viewModel.onEvent(
                            NewGameFormEvent.GameNameChanged(gameName = it)
                        )
                    },
                    isError = state.gameNameError != null,
                    label = { Text(text = "Name") },
                    placeholder = { Text(text = "My cool game") },
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        capitalization = KeyboardCapitalization.Sentences
                    ),
                    modifier = Modifier.fillMaxWidth()
                )
                Text(
                    text = state.gameNameError ?: "",
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.End,
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(20.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.Bottom,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "Select players", fontStyle = MaterialTheme.typography.h4.fontStyle)
                    Text(
                        text = "${state.selectedPlayers.size}/4",
                        fontSize = MaterialTheme.typography.h6.fontSize,
                        fontWeight = FontWeight.Bold
                    )
                }
                Divider(modifier = Modifier.padding(vertical = 12.dp))
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    item {
                        NewPlayerCard(
                            onClick = { appNavController.navigate(NavDestination.NewPlayer.route) }
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                    }
                    items(items = players, key = { it.id }) { player ->
                        SelectablePlayerCard(
                            player = player,
                            selected = state.selectedPlayers.any { it.id == player.id },
                            onClick = { viewModel.togglePlayer(player) },
                            modifier = Modifier.animateItemPlacement(
                                animationSpec = tween(durationMillis = 200)
                            )
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                    }
                }
            }
        }
    }
}

@Composable
private fun TopBar(
    saveButtonEnabled: Boolean,
    onBackButtonClicked: () -> Unit,
    onSave: () -> Unit
) {
    val saveButtonAlpha by animateFloatAsState(
        if (saveButtonEnabled) ContentAlpha.high else ContentAlpha.disabled
    )

    TopAppBar(
        title = { Text(text = "New game") },
        navigationIcon = {
            IconButton(onClick = onBackButtonClicked) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Back",
                    tint = MaterialTheme.colors.onPrimary
                )
            }
        },
        actions = {
            IconButton(onClick = onSave, enabled = saveButtonEnabled) {
                Icon(
                    imageVector = Icons.Default.Check,
                    contentDescription = "Save",
                    tint = MaterialTheme.colors.onPrimary.copy(alpha = saveButtonAlpha)
                )
            }
        },
        backgroundColor = MaterialTheme.colors.primaryVariant
    )
}

@Composable
fun SelectablePlayerCard(
    player: Player,
    selected: Boolean,
    onClick: () -> Unit,
    borderWidth: Dp = 1.dp,
    borderColor: Color = if (selected) MaterialTheme.colors.primary
    else MaterialTheme.colors.onSurface.copy(alpha = 0.2f),
    borderShape: Shape = MaterialTheme.shapes.medium,
    iconColor: Color = if (selected) MaterialTheme.colors.primary
    else MaterialTheme.colors.onSurface.copy(alpha = 0.2f),
    modifier: Modifier = Modifier
) {
    val cardScale = remember { Animatable(initialValue = 1f) }
    val iconScale = remember { Animatable(initialValue = 1f) }

    LaunchedEffect(key1 = selected) {
        if (selected) {
            launch {
                cardScale.animateTo(
                    targetValue = 0.3f,
                    animationSpec = tween(
                        durationMillis = 50
                    )
                )
                cardScale.animateTo(
                    targetValue = 1f,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            }
            launch {
                cardScale.animateTo(
                    targetValue = 0.95f,
                    animationSpec = tween(
                        durationMillis = 50
                    )
                )
                cardScale.animateTo(
                    targetValue = 1f,
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioLowBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            }
        } else {
            launch { cardScale.animateTo(1f) }
            launch { iconScale.animateTo(1f) }
        }

    }

    Column(
        modifier = modifier
            .scale(cardScale.value)
            .border(
                width = borderWidth,
                color = borderColor,
                shape = borderShape
            )
            .clip(borderShape)
            .clickable {
                onClick()
            }
    ) {
        Row(
            modifier = Modifier.padding(start = 12.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = player.name,
                fontSize = MaterialTheme.typography.h6.fontSize,
                fontWeight = FontWeight.Normal,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.weight(1f)
            )
            IconButton(onClick = onClick, modifier = Modifier.scale(iconScale.value)) {
                Icon(
                    imageVector = Icons.Default.CheckCircle,
                    contentDescription = "Check",
                    tint = iconColor
                )
            }
        }
    }
}

@Composable
fun NewPlayerCard(
    onClick: () -> Unit,
    borderWidth: Dp = 1.dp,
    borderColor: Color = MaterialTheme.colors.onSurface,
    borderShape: Shape = MaterialTheme.shapes.medium,
    iconColor: Color = MaterialTheme.colors.onSurface,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .border(
                width = borderWidth,
                color = borderColor,
                shape = borderShape
            )
            .clip(borderShape)
            .clickable {
                onClick()
            }
    ) {
        Row(
            modifier = Modifier.padding(start = 12.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "New player",
                fontSize = MaterialTheme.typography.h6.fontSize,
                fontWeight = FontWeight.Normal,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.weight(1f)
            )
            IconButton(onClick = onClick) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Add",
                    tint = iconColor
                )
            }
        }
    }
}