package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.main

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.gitlab.djsushi123.kingenscoreboard.presentation.BottomBarNavDestination
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.players.PlayersScreen
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.scoreboard.ScoreboardScreen

@Composable
fun MainScreenNavGraph(navController: NavHostController, paddingValues: PaddingValues) {
    NavHost(
        navController = navController,
        startDestination = BottomBarNavDestination.Scoreboard.route
    ) {
        composable(BottomBarNavDestination.ActiveGames.route) {
            // TODO
        }
        composable(BottomBarNavDestination.Scoreboard.route) {
            ScoreboardScreen(modifier = Modifier.padding(paddingValues))
        }
        composable(BottomBarNavDestination.Players.route) {
            PlayersScreen(modifier = Modifier.padding(paddingValues))
        }
        composable(BottomBarNavDestination.History.route) {
            // TODO
        }
    }
}