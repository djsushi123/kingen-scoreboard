package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.PlayerAbbreviationValidator
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.PlayerNameValidator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewPlayerScreenViewModel(
    private val playerRepository: PlayerRepository,
    private val playerNameValidator: PlayerNameValidator,
    private val playerAbbreviationValidator: PlayerAbbreviationValidator
) : ViewModel() {

    var state by mutableStateOf(NewPlayerScreenState())

    fun onEvent(event: NewPlayerFormEvent) {
        when (event) {
            is NewPlayerFormEvent.PlayerNameChanged -> {
                state = state.copy(playerName = event.name)
                viewModelScope.launch(Dispatchers.IO) {
                    val result = playerNameValidator.execute(event.name)
                    state = if (result.successful) state.copy(playerNameError = null)
                    else state.copy(playerNameError = result.errorMessage)
                    updateSaveButtonEnabled()
                }
            }
            is NewPlayerFormEvent.PlayerAbbreviationChanged -> {
                var abbreviation = event.abbreviation

                if (abbreviation.length > 2) return

                if (abbreviation.all { it.isLetterOrDigit() })
                    abbreviation = abbreviation.uppercase()
                else return

                viewModelScope.launch(Dispatchers.IO) {
                    val result = playerAbbreviationValidator.execute(abbreviation)
                    state = state.copy(
                        playerAbbreviation = abbreviation,
                        playerAbbreviationError = result.errorMessage
                    )
                    updateSaveButtonEnabled()
                }
            }
            is NewPlayerFormEvent.PlayerColorChanged -> {
                state = state.copy(playerColor = event.color)
            }
        }
    }

    private fun updateSaveButtonEnabled() {
        state = state.copy(
            saveButtonEnabled = state.playerNameError == null && state.playerAbbreviationError == null
        )
    }

    suspend fun savePlayer(player: Player) {
        playerRepository.insertPlayer(player)
    }
}