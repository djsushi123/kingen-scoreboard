package com.gitlab.djsushi123.kingenscoreboard.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import com.gitlab.djsushi123.kingenscoreboard.ui.theme.KingenScoreboardTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {

    private val viewModel: MainActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            KingenScoreboardTheme {
                KingenScoreboardApp(
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }
}