package com.gitlab.djsushi123.kingenscoreboard.domain.validation

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: String? = null
)