package com.gitlab.djsushi123.kingenscoreboard.data.repository

import com.gitlab.djsushi123.kingenscoreboard.data.local.dao.GameDao
import com.gitlab.djsushi123.kingenscoreboard.data.mapper.toGame
import com.gitlab.djsushi123.kingenscoreboard.data.mapper.toGameEntity
import com.gitlab.djsushi123.kingenscoreboard.data.mapper.toParticipantEntity
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Game
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Participant
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.GameRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GameRepositoryImpl(private val gameDao: GameDao) : GameRepository {

    override suspend fun getAllGames(): List<Game> {
        return gameDao.getAllGames().map { it.toGame() }
    }

    override fun getAllGamesAsFlow(): Flow<List<Game>> {
        return gameDao.getAllGamesAsFlow().map { it.map { entity -> entity.toGame() } }
    }

    override suspend fun insertGame(game: Game): Long {
        return gameDao.insertGame(game.toGameEntity())
    }

    override suspend fun insertParticipants(participants: List<Participant>) {
        gameDao.insertParticipants(participants.map { it.toParticipantEntity() })
    }
}