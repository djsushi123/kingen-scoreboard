package com.gitlab.djsushi123.kingenscoreboard.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.GameEntity
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.ParticipantEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GameDao {

    @Query("SELECT * FROM games")
    fun getAllGames(): List<GameEntity>

    @Query("SELECT * FROM games")
    fun getAllGamesAsFlow(): Flow<List<GameEntity>>

    @Insert
    suspend fun insertGame(game: GameEntity): Long

    @Insert
    suspend fun insertParticipants(participants: List<ParticipantEntity>)
}