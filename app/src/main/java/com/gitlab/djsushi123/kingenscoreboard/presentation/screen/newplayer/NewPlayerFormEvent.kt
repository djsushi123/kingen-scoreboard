package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer

import androidx.compose.ui.graphics.Color

sealed class NewPlayerFormEvent {
    data class PlayerNameChanged(val name: String) : NewPlayerFormEvent()
    data class PlayerAbbreviationChanged(val abbreviation: String) : NewPlayerFormEvent()
    data class PlayerColorChanged(val color: Color) : NewPlayerFormEvent()
}