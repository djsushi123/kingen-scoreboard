package com.gitlab.djsushi123.kingenscoreboard.data.local

import androidx.room.TypeConverter
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Round

@Suppress("UNUSED")
class Converters {

    @TypeConverter
    fun fromRound(round: Round): Int = round.ordinal

    @TypeConverter
    fun toRound(ordinal: Int): Round = Round.values()[ordinal]
}