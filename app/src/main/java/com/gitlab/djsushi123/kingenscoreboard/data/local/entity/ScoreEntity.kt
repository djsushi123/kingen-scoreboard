package com.gitlab.djsushi123.kingenscoreboard.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Round
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(
    tableName = "scores",
    primaryKeys = ["game_id", "player_id", "round_id"],
    foreignKeys = [
        ForeignKey(
            entity = GameEntity::class,
            parentColumns = ["id"],
            childColumns = ["game_id"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = PlayerEntity::class,
            parentColumns = ["id"],
            childColumns = ["player_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class ScoreEntity(
    @ColumnInfo(name = "game_id", index = true) val gameId: Long,
    @ColumnInfo(name = "player_id", index = true) val playerId: Long,
    @ColumnInfo(name = "round_id") val round: Round,
    val score: Int
) : Parcelable