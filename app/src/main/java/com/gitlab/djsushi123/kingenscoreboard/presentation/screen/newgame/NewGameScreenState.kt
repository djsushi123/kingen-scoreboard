package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame

import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player

data class NewGameScreenState(
    val saveButtonEnabled: Boolean = false,
    val gameName: String = "",
    val gameNameError: String? = "",
    val selectedPlayers: List<Player> = emptyList()
)