package com.gitlab.djsushi123.kingenscoreboard.domain.validation

import com.gitlab.djsushi123.kingenscoreboard.domain.repository.GameRepository

class GameNameValidator(private val gameRepository: GameRepository) {

    suspend fun execute(gameName: String): ValidationResult {
        var errorMessage: String? = when {
            gameName.isBlank() -> "Name cannot be blank."
            gameName.length > 32 -> "Name cannot contain more than 32 characters."
            gameName.first().isWhitespace() -> "Name cannot start with whitespace."
            gameName.last().isWhitespace() -> "Name cannot end with whitespace."
            !gameName.all { it.isLetterOrDigit() || it.isWhitespace() } ->
                "Can only contain letters, digits and spaces."
            else -> null
        }

        val games = gameRepository.getAllGames()
        val gameWithSameNameExists = games.any { it.name == gameName }
        if (gameWithSameNameExists) errorMessage = "Game with the same name already exists."

        return ValidationResult(successful = errorMessage == null, errorMessage = errorMessage)
    }

}