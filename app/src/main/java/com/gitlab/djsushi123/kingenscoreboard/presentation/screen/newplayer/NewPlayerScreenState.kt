package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer

import androidx.compose.ui.graphics.Color

data class NewPlayerScreenState(
    val saveButtonEnabled: Boolean = false,
    val playerName: String = "",
    val playerNameError: String? = "",
    val playerAbbreviation: String = "",
    val playerAbbreviationError: String? = "",
    val playerColor: Color = newPlayerColors.first()
)