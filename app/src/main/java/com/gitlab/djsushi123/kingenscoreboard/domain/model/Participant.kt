package com.gitlab.djsushi123.kingenscoreboard.domain.model

data class Participant(
    val gameId: Long,
    val playerId: Long,
    val initialOrder: Int
)