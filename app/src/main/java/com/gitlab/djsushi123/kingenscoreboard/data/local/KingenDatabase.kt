package com.gitlab.djsushi123.kingenscoreboard.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gitlab.djsushi123.kingenscoreboard.data.local.dao.GameDao
import com.gitlab.djsushi123.kingenscoreboard.data.local.dao.PlayerDao
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.GameEntity
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.ParticipantEntity
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.PlayerEntity
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.ScoreEntity

@Database(
    entities = [PlayerEntity::class, GameEntity::class, ParticipantEntity::class, ScoreEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class KingenDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao
    abstract fun gameDao(): GameDao
}