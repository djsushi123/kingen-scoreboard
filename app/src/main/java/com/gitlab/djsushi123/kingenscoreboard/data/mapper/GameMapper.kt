package com.gitlab.djsushi123.kingenscoreboard.data.mapper

import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.GameEntity
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Game

fun GameEntity.toGame() = Game(
    id = id,
    name = name
)

fun Game.toGameEntity() = GameEntity(
    id = id,
    name = name
)

