package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Game
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Participant
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.GameRepository
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.GameNameValidator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewGameScreenViewModel(
    private val playerRepository: PlayerRepository,
    private val gameRepository: GameRepository,
    private val gameNameValidator: GameNameValidator
) : ViewModel() {

    var state by mutableStateOf(NewGameScreenState())

    val playerFlow = playerRepository.getAllPlayersAsFlow()

    fun togglePlayer(player: Player) {
        // If the toggled player already is in the selected player list, deselect him/her
        state = if (state.selectedPlayers.any { it.id == player.id }) state.copy(
            selectedPlayers = state.selectedPlayers - player
            // otherwise, select him/her
        ) else if (state.selectedPlayers.size < 4) state.copy(
            selectedPlayers = state.selectedPlayers + player
        ) else state
        updateSaveButtonEnabled()
    }

    fun onEvent(event: NewGameFormEvent) {
        when (event) {
            is NewGameFormEvent.GameNameChanged -> {
                state = state.copy(gameName = event.gameName)
                viewModelScope.launch(Dispatchers.IO) {
                    val result = gameNameValidator.execute(event.gameName)
                    state = if (result.successful) state.copy(gameNameError = null)
                    else state.copy(gameNameError = result.errorMessage)
                    updateSaveButtonEnabled()
                }
            }
        }
    }

    private fun updateSaveButtonEnabled() {
        state = state.copy(
            saveButtonEnabled = state.gameNameError == null && state.selectedPlayers.size == 4
        )
    }

    suspend fun saveGameAndParticipants(game: Game, participants: List<Participant>) {
        val gameId = gameRepository.insertGame(game)
        val updatedParticipants = participants.map { it.copy(gameId = gameId) }
        gameRepository.insertParticipants(updatedParticipants)
    }
}