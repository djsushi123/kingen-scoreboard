package com.gitlab.djsushi123.kingenscoreboard.data.local.dao

import androidx.room.*
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.PlayerEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PlayerDao {

    @Insert
    suspend fun insertPlayer(player: PlayerEntity)

    @Query("SELECT * FROM players")
    suspend fun getAllPlayers(): List<PlayerEntity>

    @Query("SELECT * FROM players")
    fun getAllPlayersAsFlow(): Flow<List<PlayerEntity>>

    @Delete
    suspend fun deletePlayer(player: PlayerEntity)

    @Update
    suspend fun updatePlayer(player: PlayerEntity)

}