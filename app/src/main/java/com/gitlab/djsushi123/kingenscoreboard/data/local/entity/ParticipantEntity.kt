package com.gitlab.djsushi123.kingenscoreboard.data.local.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import kotlinx.parcelize.Parcelize

/**
 * Links the game and player tables together. Is used to define a player who is participating in a
 * game. The [initialOrder] property describes the order in which the player should play.
 */
@Parcelize
@Entity(
    tableName = "participants",
    primaryKeys = ["game_id", "player_id", "initial_order"],
    foreignKeys = [
        ForeignKey(
            entity = GameEntity::class,
            parentColumns = ["id"],
            childColumns = ["game_id"],
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = PlayerEntity::class,
            parentColumns = ["id"],
            childColumns = ["player_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class ParticipantEntity(
    @ColumnInfo(name = "game_id", index = true) val gameId: Long,
    @ColumnInfo(name = "player_id", index = true) val playerId: Long,
    @ColumnInfo(name = "initial_order") val initialOrder: Int
) : Parcelable