package com.gitlab.djsushi123.kingenscoreboard.di

import androidx.room.Room
import com.gitlab.djsushi123.kingenscoreboard.data.local.KingenDatabase
import com.gitlab.djsushi123.kingenscoreboard.data.repository.GameRepositoryImpl
import com.gitlab.djsushi123.kingenscoreboard.data.repository.PlayerRepositoryImpl
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.GameRepository
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.GameNameValidator
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.PlayerAbbreviationValidator
import com.gitlab.djsushi123.kingenscoreboard.domain.validation.PlayerNameValidator
import com.gitlab.djsushi123.kingenscoreboard.presentation.MainActivityViewModel
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame.NewGameScreenViewModel
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer.NewPlayerScreenViewModel
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.players.PlayersScreenViewModel
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.scoreboard.ScoreboardScreenViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    // KingenDatabase
    single {
        Room.databaseBuilder(
            androidContext(),
            KingenDatabase::class.java, "kingen-database"
        ).build()
    }

    // Player DAO
    single { get<KingenDatabase>().playerDao() }

    // Game DAO
    single { get<KingenDatabase>().gameDao() }

    // Player repository
    single<PlayerRepository> { PlayerRepositoryImpl(get()) }

    // Game repository
    single<GameRepository> { GameRepositoryImpl(get()) }


    // VALIDATORS
    single { GameNameValidator(get()) }
    single { PlayerNameValidator(get()) }
    single { PlayerAbbreviationValidator(get()) }

    viewModel { PlayersScreenViewModel(get()) }
    viewModel { ScoreboardScreenViewModel() }
    viewModel { NewPlayerScreenViewModel(get(), get(), get()) }
    viewModel { NewGameScreenViewModel(get(), get(), get()) }
    viewModel { MainActivityViewModel() }
}