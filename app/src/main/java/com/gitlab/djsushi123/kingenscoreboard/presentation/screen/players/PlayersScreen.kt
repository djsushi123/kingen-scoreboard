package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.players

import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.github.skydoves.colorpicker.compose.HsvColorPicker
import com.github.skydoves.colorpicker.compose.rememberColorPickerController
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PlayersScreen(
    viewModel: PlayersScreenViewModel = getViewModel(),
    modifier: Modifier = Modifier
) {
    var addPlayerDialogOpen by remember { mutableStateOf(false) }
    var expandedMenuIndex by remember { mutableStateOf<Int?>(null) }

    val players by viewModel.playerFlow.collectAsState(initial = emptyList())

    Column(
        modifier = modifier
    ) {
        LazyColumn(
            modifier = modifier
                .padding(horizontal = 4.dp)
                .fillMaxSize()
        ) {
            itemsIndexed(items = players, key = { _, player -> player.name }) { index, player ->
                PlayerCard(
                    player = player,
                    onEdit = {
                        expandedMenuIndex = null
                        // TODO
                    },
                    onDelete = {
                        expandedMenuIndex = null
                        viewModel.deletePlayer(player)
                    },
                    menuExpanded = expandedMenuIndex == index,
                    onMenuDismiss = { expandedMenuIndex = null },
                    onMenuButtonPressed = { expandedMenuIndex = index },
                    modifier = Modifier
                        .fillMaxWidth()
                        .animateItemPlacement(tween())
                )
                if (index != players.lastIndex) Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }


    if (addPlayerDialogOpen) {
        PlayerCreationDialog(
            onSave = {
                viewModel.savePlayer(it)
                addPlayerDialogOpen = false
            },
            onDismiss = {
                addPlayerDialogOpen = false
            }
        )
    }
}

@Composable
fun PlayerCard(
    player: Player,
    onEdit: () -> Unit,
    onDelete: () -> Unit,
    menuExpanded: Boolean,
    onMenuButtonPressed: () -> Unit,
    onMenuDismiss: () -> Unit,
    modifier: Modifier = Modifier
) {
    Card(modifier = modifier, shape = MaterialTheme.shapes.small) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(start = 8.dp, top = 8.dp, bottom = 8.dp)
        ) {
            // Left side
            Row(
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {
                val colorSquircleWidth = with(LocalDensity.current) { 16.dp.toPx() }
                Canvas(
                    modifier = Modifier
                        .width(16.dp)
                        .height(16.dp)
                ) {
                    drawRoundRect(
                        color = player.color,
                        cornerRadius = CornerRadius(15f, 15f),
                        size = Size(colorSquircleWidth, colorSquircleWidth)
                    )
                }
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = "${player.name} (${player.abbreviation})",
                    fontSize = 20.sp,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            }

            Box {
                IconButton(onClick = onMenuButtonPressed) {
                    Icon(Icons.Default.MoreVert, "More")
                }
                ThreeDotMenu(
                    expanded = menuExpanded,
                    onDismiss = onMenuDismiss,
                    onEdit = onEdit,
                    onDelete = onDelete
                )
            }
        }
    }
}

@Composable
fun ThreeDotMenu(
    expanded: Boolean,
    onDismiss: () -> Unit,
    onEdit: () -> Unit,
    onDelete: () -> Unit
) {
    DropdownMenu(expanded = expanded, onDismissRequest = onDismiss) {
        DropdownMenuItem(onClick = onEdit) {
            Icon(Icons.Default.Edit, "Edit")
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Edit")
        }
        DropdownMenuItem(onClick = onDelete) {
            Icon(Icons.Default.Delete, "Delete")
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Delete")
        }
    }
}

@Composable
fun PlayerCreationDialog(
    onSave: (player: Player) -> Unit,
    onDismiss: () -> Unit
) {
    var name by remember { mutableStateOf("") }
    var abbreviation by remember { mutableStateOf("") }
    var color by remember { mutableStateOf(Color.Unspecified) }

    AlertDialog(
        onDismissRequest = onDismiss,
        confirmButton = {
            Button(onClick = {
                onSave(
                    Player(
                        name = name,
                        abbreviation = abbreviation,
                        color = color
                    )
                )
            }) {
                Text(text = "Add")
            }
        },
        dismissButton = {
            Button(onClick = onDismiss) {
                Text(text = "Cancel")
            }
        },
        text = {
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "New player", fontSize = 20.sp, modifier = Modifier.fillMaxWidth())
                Divider(modifier = Modifier.padding(vertical = 8.dp))
                OutlinedTextField(
                    label = { Text(text = "Name") },
                    value = name,
                    onValueChange = { name = it }
                )
                OutlinedTextField(
                    label = { Text(text = "Abbreviation") },
                    value = abbreviation,
                    onValueChange = { abbreviation = it }
                )
                HsvColorPicker(
                    controller = rememberColorPickerController(),
                    onColorChanged = { color = it.color },
                    modifier = Modifier
                        .width(220.dp)
                        .height(220.dp)
                        .padding(10.dp)
                )
            }
        }
    )
}

@Preview
@Composable
fun PlayerCardPreview() {
    val player =
        Player(name = "Martin Gaens", abbreviation = "MA", color = Color.Red)
    PlayerCard(
        player = player,
        onEdit = {},
        onDelete = {},
        menuExpanded = false,
        onMenuButtonPressed = {},
        onMenuDismiss = {}
    )
}