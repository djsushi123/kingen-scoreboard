package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.players

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player
import com.gitlab.djsushi123.kingenscoreboard.domain.repository.PlayerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PlayersScreenViewModel(
    private val playerRepository: PlayerRepository
) : ViewModel() {

    val playerFlow = playerRepository.getAllPlayersAsFlow()

    fun savePlayer(player: Player) {
        viewModelScope.launch(Dispatchers.IO) {
            playerRepository.insertPlayer(player)
        }
    }

    fun deletePlayer(player: Player) {
        viewModelScope.launch(Dispatchers.IO) {
            playerRepository.deletePlayer(player)
        }
    }
}