package com.gitlab.djsushi123.kingenscoreboard.presentation.screen.scoreboard

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun RowScope.Cell(
    text: String = "",
    onClick: (() -> Unit)? = null,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .weight(1f)
            .fillMaxHeight()
            .border(width = 1.dp, color = Color.Gray)
            .apply { if (onClick != null) clickable(onClick = onClick) },
        contentAlignment = Alignment.Center
    ) {
        val textStyleH3 = MaterialTheme.typography.h3
        var textStyle by remember { mutableStateOf(textStyleH3) }
        var readyToDraw by remember { mutableStateOf(false) }
        Text(
            text = text,
            style = textStyle,
            softWrap = false,
            maxLines = 1,
            modifier = modifier.padding(6.dp).drawWithContent { if (readyToDraw) drawContent() },
            onTextLayout = { textLayoutResult ->
                if (textLayoutResult.didOverflowHeight || textLayoutResult.didOverflowWidth) {
                    textStyle = textStyle.copy(fontSize = textStyle.fontSize * 0.9)
                } else {
                    readyToDraw = true
                }
            }
        )
    }
}

@Composable
fun ColumnScope.GridRow(content: @Composable RowScope.() -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .weight(1f)
    ) {
        content()
    }
}

@Preview(widthDp = 64, heightDp = 32)
@Composable
fun CellPreview() {
    Row {
        Cell(
            text = "MA",
            modifier = Modifier.background(Color.Yellow)
        )
    }
}