package com.gitlab.djsushi123.kingenscoreboard.data.mapper

import androidx.compose.ui.graphics.toArgb
import com.gitlab.djsushi123.kingenscoreboard.data.local.entity.PlayerEntity
import com.gitlab.djsushi123.kingenscoreboard.domain.model.Player

fun PlayerEntity.toPlayer() = Player(
    id = id,
    name = name,
    abbreviation = abbreviation,
    color = androidx.compose.ui.graphics.Color(color)
)

fun Player.toPlayerEntity() = PlayerEntity(
    id = id,
    name = name,
    abbreviation = abbreviation,
    color = color.toArgb()
)