package com.gitlab.djsushi123.kingenscoreboard.presentation

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.main.MainScreen
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newgame.NewGameScreen
import com.gitlab.djsushi123.kingenscoreboard.presentation.screen.newplayer.NewPlayerScreen
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun KingenScoreboardApp(
    modifier: Modifier = Modifier
) {
    val systemUiController = rememberSystemUiController()

    val primaryVariantColor = MaterialTheme.colors.primaryVariant
    val darkIcons = MaterialTheme.colors.isLight

    LaunchedEffect(key1 = true) {
        systemUiController.setStatusBarColor(
            color = primaryVariantColor,
            darkIcons = darkIcons
        )
    }

    val appNavController = rememberAnimatedNavController()
    val navController = rememberNavController()

    AnimatedNavHost(
        navController = appNavController,
        startDestination = NavDestination.Main.route,
        enterTransition = {
            slideIntoContainer(towards = AnimatedContentScope.SlideDirection.Left)
        },
        popEnterTransition = {
            slideIntoContainer(towards = AnimatedContentScope.SlideDirection.Right)
        },
        exitTransition = {
            slideOutOfContainer(towards = AnimatedContentScope.SlideDirection.Left)
        },
        popExitTransition = {
            slideOutOfContainer(towards = AnimatedContentScope.SlideDirection.Right)
        }
    ) {
        composable(route = NavDestination.Main.route) {
            MainScreen(
                appNavController = appNavController,
                navController = navController,
                modifier = modifier
            )
        }
        composable(route = NavDestination.NewGame.route) {
            NewGameScreen(
                appNavController = appNavController,
                modifier = modifier
            )
        }
        composable(route = NavDestination.NewPlayer.route) {
            NewPlayerScreen(
                appNavController = appNavController,
                modifier = modifier
            )
        }
    }
}