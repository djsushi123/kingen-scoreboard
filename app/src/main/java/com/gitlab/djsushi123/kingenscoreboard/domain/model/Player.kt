package com.gitlab.djsushi123.kingenscoreboard.domain.model

import androidx.compose.ui.graphics.Color

/**
 * The model class representing a player. The [name] parameter is the player's full name. It must
 * be unique. The [abbreviation] is a two-letter short abbreviation of the player's name and [color]
 * is his color which will be his color in the scoreboard.
 */
data class Player(
    val id: Long = 0,
    val name: String,
    val abbreviation: String,
    val color: Color
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Player

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}